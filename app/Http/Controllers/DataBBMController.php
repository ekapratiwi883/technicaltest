<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DataBBM;
use App\Models\Kendaraan;

class DataBBMController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kendaraan = Kendaraan::all();
        $bbm = DataBBM::with('bbmKendaraan')->get();
        return view('bbm.index', compact('bbm', 'kendaraan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'km_awal' => 'required',
            'km_akhir' => 'required',
            'bbm' => 'required',
        ]);
        $konsumsi_bbm = ($request->km_akhir - $request->km_awal)/$request->bbm;

        DataBBM::create([
            'kendaraan_id' => $request->kendaraan_id,
            'km_awal' => $request->km_awal,
            'km_akhir' => $request->km_akhir,
            'bbm' => $request->bbm,
            'konsumsi_bbm' => $konsumsi_bbm,
        ]);
        return redirect()->route('bbm.index')->with('success', 'Data Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'km_awal' => 'required',
            'km_akhir' => 'required',
            'bbm' => 'required',
        ]);

        $bbm = DataBBM::find($id);
        $konsumsi_bbm = ($request->km_akhir - $request->km_awal)/$request->bbm;

        $bbm->update([
            'kendaraan_id' => $request->kendaraan_id,
            'km_awal' => $request->km_awal,
            'km_akhir' => $request->km_akhir,
            'bbm' => $request->bbm,
            'konsumsi_bbm' => $konsumsi_bbm,
        ]);
        return redirect()->route('bbm.index')->with('success', 'Data Berhasil Diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $bbm = DataBBM::find($id);
        $bbm->delete();

        return redirect()->route('bbm.index')->with('success', 'Data Berhasil Dihapus!');
    }
}
