<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kendaraan;

class KendaraanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kendaraan = Kendaraan::all();
        return view('kendaraan.index', compact('kendaraan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'merek_tipe' => 'required',
            'plat_nomor' => 'required',
        ]);
        Kendaraan::create([
            'merek_tipe' => $request->merek_tipe,
            'plat_nomor' => $request->plat_nomor,
            'kategori' => $request->kategori,
        ]);
        return redirect()->route('kendaraan.index')->with('success', 'Data Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        
        
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'merek_tipe' => 'required',
            'plat_nomor' => 'required',
        ]);
        $kendaraan = Kendaraan::find($id);
        $kendaraan->update([
            'merek_tipe' => $request->merek_tipe,
            'plat_nomor' => $request->plat_nomor,
            'kategori' => $request->kategori,
        ]);
        return redirect()->route('kendaraan.index')->with('success', 'Data Berhasil Diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $kendaraan = Kendaraan::find($id);
        $kendaraan->delete();

        return redirect()->route('kendaraan.index')->with('success', 'Data Berhasil Dihapus!');
        
    
    }
}
