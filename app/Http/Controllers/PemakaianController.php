<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kendaraan;
use App\Models\Pemakaian;

class PemakaianController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kendaraan = Kendaraan::all();
        $data_pemakaian = Pemakaian::with('pemakaian')->get();
        return view('pemakaian.index', compact('kendaraan', 'data_pemakaian'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_driver' => 'required',
            'keperluan' => 'required',
            'tanggal_awal' => 'required',
            'tanggal_akhir' => 'required',
        ]);
        Pemakaian::create([
            'kendaraan_id' => $request->kendaraan_id,
            'nama_driver' => $request->nama_driver,
            'keperluan' => $request->keperluan,
            'tanggal_awal' => $request->tanggal_awal,
            'tanggal_akhir' => $request->tanggal_akhir,
            'keterangan' => $request->keterangan,
        ]);
        return redirect()->route('pemakaian.index')->with('success', 'Data Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'nama_driver' => 'required',
            'keperluan' => 'required',
            'tanggal_awal' => 'required',
            'tanggal_akhir' => 'required',
        ]);
        $pemakaian = Pemakaian::find($id);
        $pemakaian->update([
            'kendaraan_id' => $request->kendaraan_id,
            'nama_driver' => $request->nama_driver,
            'keperluan' => $request->keperluan,
            'tanggal_awal' => $request->tanggal_awal,
            'tanggal_akhir' => $request->tanggal_akhir,
            'keterangan' => $request->keterangan,
        ]);
        return redirect()->route('pemakaian.index')->with('success', 'Data Berhasil Diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $pemakaian = Pemakaian::find($id);
        $pemakaian->delete();

        return redirect()->route('pemakaian.index')->with('success', 'Data Berhasil Dihapus!');
    }
}
