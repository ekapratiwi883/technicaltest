<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;
use Illuminate\Support\Facades\Storage;
use App\Models\Kendaraan;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kendaraan = Kendaraan::all();
        $service = Service::with('serviceKendaraan')->get();
        return view('service.index', compact('service', 'kendaraan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'penanggung_jawab' => 'required',
            'tanggal' => 'required',
            'jenis_service' => 'required',
            'biaya' => 'required',
            'nota' => 'required|mimes: jpg,jpeg,png'
        ]);

        $file_name = $request->file('nota')->getClientOriginalName();
       $request->file('nota')->move('upload/', $file_name);

        Service::create([
            'kendaraan_id' => $request->kendaraan_id,
            'tanggal' => $request->tanggal,
            'penanggung_jawab' => $request->penanggung_jawab,
            'jenis_service' => $request->jenis_service,
            'biaya' => $request->biaya,
            'nota' => $file_name,
        ]);
        return redirect()->route('service.index')->with('success', 'Data Berhasil Disimpan!');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'penanggung_jawab' => 'required',
            'tanggal' => 'required',
            'jenis_service' => 'required',
            'biaya' => 'required',
            'nota' => 'required||mimes: jpg,jpeg,png'
        ]);

        if (empty($request->file('nota'))) {
            $data = Service::find($id);
            $data->update([
                'kendaraan_id' => $request->kendaraan_id,
                'tanggal' => $request->tanggal,
                'penanggung_jawab' => $request->penanggung_jawab,
                'jenis_service' => $request->jenis_service,
                'biaya' => $request->biaya,
            ]);
            return redirect()->route('service.index')->with('success', 'Data Berhasil Diubah!');
        } else {
            $data = Service::find($id);
            Storage::delete($data->nota);
            $file_name = $request->file('nota')->getClientOriginalName();
            $request->file('nota')->move('upload/', $file_name);
            $data->update([
                'kendaraan_id' => $request->kendaraan_id,
                'tanggal' => $request->tanggal,
                'penanggung_jawab' => $request->penanggung_jawab,
                'jenis_service' => $request->jenis_service,
                'biaya' => $request->biaya,
                'nota' => $file_name
            ]);
            return redirect()->route('service.index')->with('success', 'Data Berhasil Diubah!');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $service = Service::find($id);
        $service->delete();

        return redirect()->route('service.index')->with('success', 'Data Berhasil Dihapus!');
    }
}
