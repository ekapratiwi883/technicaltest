<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataBBM extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'kendaraan_id',
        'km_awal',
        'km_akhir',
        'bbm',
        'konsumsi_bbm'
    ];
    protected $primaryKey='id';
    protected $table = 'data_bbm';
    public function bbmKendaraan()
    {
        return $this->belongsTo(Kendaraan::class, 'kendaraan_id');
    }
}
