<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kendaraan extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'merek_tipe',
        'plat_nomor',
        'kategori'
    ];
    protected $primaryKey='id';
    protected $table = 'kendaraan';
    public function serviceKendaraan()
    {
        return $this->belongsTo(Service::class);
    }
    public function bbmKendaraan()
    {
        return $this->belongsTo(DataBBM::class);
    }
    public function pemakaian()
    {
        return $this->belongsTo(Pemakaian::class);
    }
}
