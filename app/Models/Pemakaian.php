<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemakaian extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'kendaraan_id',
        'nama_driver',
        'keperluan',
        'tanggal_awal',
        'tanggal_akhir',
        'keterangan'
    ];
    protected $primaryKey='id';
    protected $table = 'data_pemakaian';
    public function pemakaian()
    {
        return $this->belongsTo(Kendaraan::class, 'kendaraan_id');
    }
}
