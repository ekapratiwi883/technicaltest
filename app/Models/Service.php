<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'kendaraan_id',
        'tanggal',
        'penanggung_jawab',
        'jenis_service',
        'biaya',
        'nota'
    ];
    protected $primaryKey='id';
    protected $table = 'data_service';
    public function serviceKendaraan()
    {
        return $this->belongsTo(Kendaraan::class, 'kendaraan_id');
    }
}
