<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_service', function (Blueprint $table) {
            $table->id();
            $table->string('kendaraan_id');
            $table->date('tanggal');
            $table->string('penanggung_jawab');
            $table->string('jenis_service');
            $table->double('biaya');
            $table->string('nota');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_service');
    }
};
