<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_bbm', function (Blueprint $table) {
            $table->id();
            $table->integer('kendaraan_id');
            $table->double('km_awal');
            $table->double('km_akhir');
            $table->double('bbm');
            $table->double('konsumsi_bbm');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_bbm');
    }
};
