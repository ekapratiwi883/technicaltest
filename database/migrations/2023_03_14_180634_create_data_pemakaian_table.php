<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_pemakaian', function (Blueprint $table) {
            $table->id();
            $table->integer('kendaraan_id');
            $table->string('nama_driver');
            $table->string('keperluan');
            $table->datetime('tanggal_awal');
            $table->datetime('tanggal_akhir');
            $table->string('keterangan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_pemakaian');
    }
};
