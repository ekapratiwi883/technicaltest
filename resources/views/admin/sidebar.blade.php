<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SB Admin <sup>2</sup></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{url('/home')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" href="{{url('/users')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Users</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('/kendaraan')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Kendaraan</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('/bbm')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Data BBM</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('/service')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Data Service</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{url('/pemakaian')}}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Data Pemakaian Kendaraan</span></a>
    </li>

</ul>