@foreach ($bbm as $item)
<div class="modal fade" id="editModal{{$item->id}}" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('bbm.update', $item->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
        <div class="modal-body">
            <div class="form-group">
                <label for="exampleFormControlSelect1">Tipe Kendaraan</label>
                <select class="form-control" name="kendaraan_id">
                    <option value="{{$item->kendaraan_id}}" selected>{{$item->bbmKendaraan->merek_tipe}}&nbsp;-&nbsp;{{$item->bbmKendaraan->plat_nomor}}</option>
                  @foreach ($kendaraan as $data)
                  <option value="{{$data->id}}">{{$data->merek_tipe}}&nbsp;-&nbsp;{{$data->plat_nomor}}</option> 
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">KM Awal</label>
                <input type="text" class="form-control" name="km_awal" value="{{$item->km_awal}}" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">KM Akhir</label>
                <input type="text" class="form-control" name="km_akhir" value="{{$item->km_akhir}}" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">BBM (Liter)</label>
                <input type="text" class="form-control" name="bbm" value="{{$item->bbm}}" required>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
  @endforeach