@extends('admin.layout')
@section('css')
<link href="{{asset('template/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection

@section('content')
    
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Pemakaian BBM</h1>
    </div>

   

         <!-- DataTales Example -->
         <div class="card shadow mb-4">
             <div class="card-header py-3">
                 <h6 class="m-0 font-weight-bold text-primary">DataTables
                 <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
                    + Data Pemakaian BBM
                  </button>
                </h6>
             </div>
             <div class="card-body">
                 <div class="table-responsive">
                     <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                         <thead>
                             <tr>
                                 <th>Merk/Tipe</th>
                                 <th>Plat Nomor</th>
                                 <th>KM Awal</th>
                                 <th>KM Akhir</th>
                                 <th>BBM (Liter)</th>
                                 <th>Konsumsi BBM (KM/Liter)</th>
                                 <th>Action</th>
                            </tr>
                         </thead>
                         <tbody>
                            @foreach ($bbm as $item)
                                <tr>
                                    <td>{{$item->bbmKendaraan->merek_tipe}}</td>
                                    <td>{{$item->bbmKendaraan->plat_nomor}}</td>
                                    <td>{{$item->km_awal}}&nbsp;KM</td>
                                    <td>{{$item->km_akhir}}&nbsp;KM</td>
                                    <td>{{$item->bbm}}</td>
                                    <td>{{$item->konsumsi_bbm}}</td>
                                    <td class="text-center" >
                                        <a href="#" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editModal{{$item->id}}">edit</a>
                                        <form class="d-inline" method="POST" action="{{ route('bbm.destroy', $item->id) }}">
                                            @csrf
                                            @method('Delete')
                                            <button type="submit" class="btn btn-danger btn-sm">hapus</button>
                                        </form>
                                       
                                    </td>
                                </tr>
                            @endforeach
                         </tbody>
                     </table>
                
             </div>
         </div>
    </div>

</div>


  
  <!-- Modal Add -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('bbm.store')}}" method="POST">
            @csrf
        <div class="modal-body">
            <div class="form-group">
                <label for="exampleInputPassword1">Merek/Tipe Kendaraan</label>
                <select class="form-control" name="kendaraan_id">
                    <option selected>Pilih Kendaraan</option>
                  @foreach ($kendaraan as $item)
                  <option value="{{$item->id}}">{{$item->merek_tipe}}&nbsp;-&nbsp;{{$item->plat_nomor}}</option> 
                  @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">KM Awal</label>
                <input type="text" class="form-control" name="km_awal" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">KM Akhir</label>
                <input type="text" class="form-control" name="km_akhir" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">BBM (Liter)</label>
                <input type="text" class="form-control" name="bbm" required>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>

@include('sweetalert::alert')
@include('bbm.edit')
@endsection

@section('script')
<script src="{{asset('template/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('template/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('template/js/demo/datatables-demo.js')}}"></script>


@endsection