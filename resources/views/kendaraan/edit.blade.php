@foreach ($kendaraan as $item)
<div class="modal fade" id="editModal{{$item->id}}" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('kendaraan.update', $item->id)}}" method="POST">
            @csrf
            @method('PUT')
        <div class="modal-body">
            <div class="form-group">
                <label for="exampleInputPassword1">Merek/Tipe Kendaraan</label>
                <input type="text" class="form-control" name="merek_tipe" value="{{$item->merek_tipe}}">
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Plat Nomor</label>
                <input type="text" class="form-control" name="plat_nomor" value="{{$item->plat_nomor}}">
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Kategori Kendaraan</label>
                <select class="form-control" name="kategori">
                  <option value="Motor" {{ ("Motor" == $item->kategori) ? 'selected' : '' }}>Motor</option>
                  <option value="Mobil" {{ ("Mobil" == $item->kategori) ? 'selected' : '' }}>Mobil</option>
                  <option value="Alat Berat" {{ ("Alat Berat" == $item->kategori) ? 'selected' : '' }}>Alat Berat</option>
                  <option value="Mobil Angkutan Barang" {{ ("Mobil Angkutan Barang" == $item->kategori) ? 'selected' : '' }}>Mobil Angkutan Barang</option>
                </select>
              </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
  @endforeach