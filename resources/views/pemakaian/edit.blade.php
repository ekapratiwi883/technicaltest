@foreach ($data_pemakaian as $item)
<div class="modal fade" id="editModal{{$item->id}}" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('pemakaian.update', $item->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="modal-body">
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Tipe Kendaraan</label>
                    <select class="form-control" name="kendaraan_id">
                        <option value="{{$item->kendaraan_id}}" selected>{{$item->pemakaian->merek_tipe}}&nbsp;-&nbsp;{{$item->pemakaian->plat_nomor}}</option>
                    @foreach ($kendaraan as $data)
                    <option value="{{$data->id}}">{{$data->merek_tipe}}&nbsp;-&nbsp;{{$data->plat_nomor}}</option> 
                    @endforeach
                    </select>
                </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Nama Driver</label>
                    <input type="text" class="form-control" name="nama_driver" value="{{$item->nama_driver}}" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Keperluan</label>
                    <input type="text" class="form-control" name="keperluan" value="{{$item->keperluan}}" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Waktu Pemakaian</label>
                    <input type="datetime-local" class="form-control" name="tanggal_awal" value="{{$item->tanggal_awal}}" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Waktu Selesai</label>
                    <input type="datetime-local" class="form-control" name="tanggal_akhir" value="{{$item->tanggal_akhir}}" required>
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Keterangan</label>
                    <select class="form-control" name="keterangan">
                      <option value="Belum diproses" selected>Belum diproses</option>
                      <option value="Disetujui">Disetujui</option>
                      <option value="Ditolak">Ditolak</option>
                    </select>
                </div>
                
            </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
  @endforeach