@foreach ($service as $item)
<div class="modal fade" id="editModal{{$item->id}}" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('service.update', $item->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
        <div class="modal-body">
            <div class="form-group">
                <label for="exampleFormControlSelect1">Tipe Kendaraan</label>
                <select class="form-control" name="kendaraan_id">
                    <option value="{{$item->kendaraan_id}}" selected>{{$item->serviceKendaraan->merek_tipe}}&nbsp;-&nbsp;{{$item->serviceKendaraan->plat_nomor}}</option>
                  @foreach ($kendaraan as $data)
                  <option value="{{$data->id}}">{{$data->merek_tipe}}&nbsp;-&nbsp;{{$data->plat_nomor}}</option> 
                  @endforeach
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Tanggal</label>
                <input type="date" class="form-control" name="tanggal" value="{{$item->tanggal}}" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Penanggungjawab</label>
                <input type="text" class="form-control" name="penanggung_jawab" value="{{$item->penanggung_jawab}}" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Jenis Service</label>
                <input type="text" class="form-control" name="jenis_service" value="{{$item->jenis_service}}" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Biaya</label>
                <input type="text" class="form-control" name="biaya" value="{{$item->biaya}}" required>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Nota</label>
                <input type="file" class="form-control" name="nota" value="{{$item->nota}}" required>
                <img src="{{asset('upload/'.$item->nota) }}" width="120px">
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
  @endforeach