@foreach ($user as $item)
<div class="modal fade" id="editModal{{$item->id}}" tabindex="-1" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('users.update', $item->id)}}" method="POST">
            @csrf
            @method('PUT')
        <div class="modal-body">
            <div class="form-group">
                <label for="exampleInputPassword1">Nama</label>
                <input type="text" class="form-control" name="name" value="{{$item->name}}" required>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Email</label>
                <input type="text" class="form-control" readonly value="{{$item->email}}" name="email" required>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" value="{{$item->password}}" name="password" required>
              </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Role</label>
                <select class="form-control" name="role">
                  <option value="Admin" {{ ("Admin" == $item->kategori) ? 'selected' : '' }}>Admin</option>
                  <option value="Pihak Pengelola" {{ ("Pihak Pengelola" == $item->kategori) ? 'selected' : '' }}>Pihak Pengelola</option>
                </select>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>
  @endforeach