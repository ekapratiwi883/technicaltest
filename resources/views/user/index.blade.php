@extends('admin.layout')
@section('css')
<link href="{{asset('template/vendor/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection

@section('content')
    
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Data Users</h1>
    </div>

   

         <!-- DataTales Example -->
         <div class="card shadow mb-4">
             <div class="card-header py-3">
                 <h6 class="m-0 font-weight-bold text-primary">DataTables
                 <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
                    + Data Users
                  </button>
                </h6>
             </div>
             <div class="card-body">
                 <div class="table-responsive">
                     <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                         <thead>
                             <tr>
                                 <th>Nama</th>
                                 <th>Email</th>
                                 <th>Role</th>
                                 <th>Action</th>
                            </tr>
                         </thead>
                         <tbody>
                            @foreach ($user as $item)
                                <tr>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>
                                        @if($item->role == "Admin")
                                            <button class="btn btn-warning btn-sm" style="width: 100%" disabled>Admin</button>
                                        @elseif($item->role == "Pihak Pengelola")
                                            <button class="btn btn-success btn-sm" style="width: 100%" disabled>Pihak Pengelola</button>
                                        @endif
                                    </td>
                                    <td class="text-center" >
                                        <a href="#" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editModal{{$item->id}}">edit</a>
                                        <form class="d-inline" method="POST" action="{{ route('users.destroy', $item->id) }}">
                                            @csrf
                                            @method('Delete')
                                            <button type="submit" class="btn btn-danger btn-sm">hapus</button>
                                        </form>
                                       
                                    </td>
                                </tr>
                            @endforeach
                         </tbody>
                     </table>
                
             </div>
         </div>
    </div>

</div>


  
  <!-- Modal Add -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form action="{{ route('users.store')}}" method="POST">
            @csrf
        <div class="modal-body">
              <div class="form-group">
                <label for="exampleInputPassword1">Nama</label>
                <input type="text" class="form-control" name="name1" required>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Email</label>
                <input type="text" class="form-control" name="email" required>
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" name="password" required>
              </div>
              {{-- <div class="form-group">
                <label for="exampleInputPassword1">Confirm Password</label>
                <input type="password" class="form-control form-control-user" name="password_confirmation" required>
              </div> --}}
            <div class="form-group">
                <label for="exampleInputPassword1">Role</label>
                <select class="form-control" name="role">
                  <option value="Admin">Admin</option>
                  <option value="Pihak Pengelola">Pihak Pengelola</option>
                </select>
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
    </form>
      </div>
    </div>
  </div>

@include('sweetalert::alert')
@include('user.edit')
@endsection

@section('script')
<script src="{{asset('template/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('template/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('template/js/demo/datatables-demo.js')}}"></script>


@endsection