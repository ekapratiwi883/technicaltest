<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KendaraanController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\DataBBMController;
use App\Http\Controllers\PemakaianController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('admin.layout');
// });

// Route::get('/home', [HomeController::class, 'index']);

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    
});

Route::get('/home', [HomeController::class, 'index']);
Route::resource('kendaraan', KendaraanController::class);
Route::resource('service', ServiceController::class);
Route::resource('bbm', DataBBMController::class);
Route::resource('pemakaian', PemakaianController::class);
Route::resource('users', UserController::class);